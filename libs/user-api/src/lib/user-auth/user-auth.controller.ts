import { Controller, Post, Body } from '@nestjs/common';
import { UserAuthService } from './user-auth.service';
import { ApiImplicitBody, ApiOperation } from '@nestjs/swagger';
import { LoginUserDto } from './login.user.dto';
import { Observable, from } from 'rxjs';

@Controller('user-auth')
export class UserAuthController {
  constructor(private readonly userAuthService: UserAuthService) {}
  @Post()
  @ApiImplicitBody({
    name: 'login user dto',
    type: LoginUserDto
  })
  @ApiOperation({ title: 'User Login' })
  login(@Body() loginUserDto: LoginUserDto): Observable<string> {
    return from(this.userAuthService.login(loginUserDto));
  }
}
