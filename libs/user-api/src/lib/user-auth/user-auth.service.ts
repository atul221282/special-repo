import { Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginUserDto } from './login.user.dto';
import { Observable } from 'rxjs';
import { UserRepository } from '@special-repo/auth-service';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload, Users } from '@special-repo/special-model';

@Injectable()
export class UserAuthService {
  constructor(
    private jwtService: JwtService,
    private userRepo: UserRepository
  ) {}

  async login(loginUserDto: LoginUserDto): Promise<string> {

    const isValid = await this.userRepo.validateUser(
      loginUserDto.email,
      loginUserDto.password
    );

    if (!isValid) throw new UnauthorizedException();
    const token = await this.signIn(loginUserDto.email);

    console.log(`Access token value is ${token}`);

    return token;
  }

  private async signIn(email: string): Promise<string> {
    // In the real-world app you shouldn't expose this method publicly
    // instead, return a token once you verify user credentials
    const payload: JwtPayload = { email: email };
    return this.jwtService.sign(payload);
  }

  async validateUser(payload: JwtPayload): Promise<Users> {
    return await this.userRepo.findOneByEmail(payload.email);
  }
}
