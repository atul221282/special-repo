import { Test, TestingModule } from '@nestjs/testing';
import { UserAuthController } from './user-auth.controller';

describe('UserAuth Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [UserAuthController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: UserAuthController = module.get<UserAuthController>(UserAuthController);
    expect(controller).toBeDefined();
  });
});
