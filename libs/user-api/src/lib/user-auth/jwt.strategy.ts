import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy, AbstractStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from '@special-repo/special-model';
import { UserRepository } from '@special-repo/auth-service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userRepo: UserRepository) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'secretKey'
    });
  }

  async validate(payload: JwtPayload) {
    console.log(`${payload.email}`);
    //TODO: Remove this check as it is hitting database everytime
    const user = await this.userRepo.findOneByEmail(payload.email);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
