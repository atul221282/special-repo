import { Controller, Post, Body, Req, Headers } from '@nestjs/common';
import { ApiImplicitBody, ApiOperation } from '@nestjs/swagger';
import { from, Observable } from 'rxjs';
import { UsersDto, UsersToken } from '@special-repo/special-model';
import { RegisterUserDto } from './register.user.dto';
import { RegisterUserService } from './register-user.service';

@Controller('register-user')
export class RegisterUserController {
  constructor(private readonly registerUser: RegisterUserService) {}
  @Post()
  @ApiImplicitBody({
    name: 'register user dto',
    type: RegisterUserDto
  })
  @ApiOperation({ title: 'Create new user' })
  create(
    @Body() registerUser: RegisterUserDto,
    @Headers() headers
  ): Observable<UsersDto> {
    return from(this.registerUser.create(registerUser));
  }
}
