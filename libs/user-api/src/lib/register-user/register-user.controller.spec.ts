import { Test, TestingModule } from '@nestjs/testing';
import { RegisterUserController } from './register-user.controller';

describe('RegisterUser Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [RegisterUserController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: RegisterUserController = module.get<RegisterUserController>(RegisterUserController);
    expect(controller).toBeDefined();
  });
});
