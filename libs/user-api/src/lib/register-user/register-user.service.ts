import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { RegisterUserDto } from './register.user.dto';
import { UserRepository, Users } from '@special-repo/auth-service';
import { UsersDto } from '@special-repo/special-model';

@Injectable()
export class RegisterUserService {
  constructor(private userRepo: UserRepository) {}

  async create(model: RegisterUserDto): Promise<UsersDto> {
    const timeStamp = new Date();

    const user: Users = {
      id: undefined,
      createBy: undefined,
      createDate: undefined,
      email: model.email,
      firstName: model.firstName,
      lastName: model.lastName,
      tokens: undefined,
      updateBy: undefined,
      updateDate: timeStamp,
      password: model.password,
      passwordHashingAlgorithm: undefined,
      passwordSalt: undefined
    };

    const createdUser = await this.userRepo.create(user);

    return createdUser;
  }
}
