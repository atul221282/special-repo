import { Module } from '@nestjs/common';
import { UsersController } from './user-api/users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@special-repo/auth-service';
import { RegisterUserController } from './register-user/register-user.controller';
import { RegisterUserService } from './register-user/register-user.service';
import { UserAuthController } from './user-auth/user-auth.controller';
import { UserAuthService } from './user-auth/user-auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { JwtStrategy } from './user-auth/jwt.strategy';
@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: 'secretKey',
      signOptions: {
        expiresIn: 3600
      }
    }),
    AuthModule
  ],
  controllers: [UsersController, RegisterUserController, UserAuthController],
  providers: [RegisterUserService, UserAuthService, JwtStrategy]
})
export class UsersModule {}
