import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Inject,
  Delete,
  UseGuards,
  Req
} from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiImplicitBody } from '@nestjs/swagger';
import { UserRepository } from '@special-repo/auth-service';
import { UsersDto, UsersToken } from '@special-repo/special-model';
import { Observable } from 'rxjs/internal/Observable';
import { from } from 'rxjs/internal/observable/from';
import { AuthService } from '@special-repo/auth-service';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
  constructor(
    private readonly userService: UserRepository,
    private readonly authService: AuthService
  ) {}

  @Get()
  @ApiImplicitBody({
    name: 'special entity data',
    type: UsersDto
  })
  @ApiOperation({ title: 'get all specials' })
  getAll(): Observable<UsersDto[]> {
    return from(this.userService.findAll(['tokens']));
  }

  @Post()
  @ApiImplicitBody({
    name: 'special entity data',
    type: UsersDto
  })
  @ApiOperation({ title: 'cerate new special' })
  create(@Body() usersDto: UsersDto): Observable<UsersToken> {
    return from(this.authService.save());
  }

  @UseGuards(AuthGuard())
  @Get(':id')
  @ApiImplicitBody({
    name: 'special entity data',
    type: UsersDto
  })
  @ApiOperation({ title: 'get user by id' })
  getById(@Param('id') id: number): Observable<UsersDto> {
    return from(this.userService.findById(id));
  }

  @Put(':id')
  @ApiImplicitBody({
    name: 'special entity data',
    type: UsersDto
  })
  @ApiOperation({ title: 'update special' })
  update(
    @Param('id') id: string,
    @Body() specialEntity: UsersDto
  ): Observable<UsersDto> {
    return from(this.userService.update(specialEntity));
  }

  @Delete(':id')
  @ApiImplicitBody({
    name: 'special entity data',
    type: UsersDto
  })
  @ApiOperation({ title: 'update special' })
  async delete(@Param('id') id: string) {
    return await this.userService.delete(+id);
  }
}
