import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Base } from './base';
import { Users } from './users';
@Entity('UsersToken')
export class UsersToken extends Base {
  @Column({
    nullable: false
  })
  token: string;

  @ManyToOne(type => Users, user =>  user.tokens, { nullable: false })
  user: Users;
}
