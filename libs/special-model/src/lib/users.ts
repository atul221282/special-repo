import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Base } from './base';
import { Exclude } from 'class-transformer';
import { UsersToken } from './users-token';

@Entity('Users')
export class Users extends Base {
  @Column({
    nullable: false,
    length: 500
  })
  email: string;

  @Column({
    nullable: false,
    length: 500
  })
  firstName: string;

  @Column({
    nullable: false,
    length: 500
  })
  lastName: string;

  @Column({
    nullable: false,
    length: 500
  })
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({
    nullable: false,
    length: 500
  })
  @Exclude({ toPlainOnly: true })
  passwordSalt: string;

  @Column({
    nullable: false,
    length: 500
  })
  @Exclude({ toPlainOnly: true })
  passwordHashingAlgorithm: string;

  @OneToMany(type => UsersToken, token => token.user,{})
  tokens: UsersToken[];
}
