import { IsNotEmpty, Validate } from 'class-validator';
import { IsDateGreaterThan } from '@special-repo/common-api';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { BaseDto } from './base.dto';

export class SpecialDto extends BaseDto {
  @ApiModelProperty()
  @IsNotEmpty()
  name: string;

  @ApiModelPropertyOptional()
  description: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @Validate(IsDateGreaterThan, ['startDate'], {
    message: 'Start date cannot be greater than end date'
  })
  endDate: Date;

  @ApiModelProperty()
  startDate: Date;
}
