import { ApiModelProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty } from 'class-validator';

export class BaseDto {
  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  @IsNotEmpty()
  createDate: Date;

  @ApiModelProperty()
  @IsNotEmpty()
  updateDate: Date;

  @ApiModelProperty()
  @IsNotEmpty()
  createBy: string;

  @ApiModelProperty()
  @IsNotEmpty()
  updateBy: string;
}
