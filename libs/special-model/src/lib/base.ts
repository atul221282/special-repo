import { PrimaryGeneratedColumn, Column } from 'typeorm';

export class Base {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'timestamp with time zone',
    nullable: false
  })
  createDate: Date;

  @Column({
    type: 'timestamp with time zone',
    nullable: false
  })
  updateDate: Date;

  @Column({
    nullable: false,
    length: 500
  })
  createBy: string;

  @Column({
    nullable: false,
    length: 500
  })
  updateBy: string;
}
