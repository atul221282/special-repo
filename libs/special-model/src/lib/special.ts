import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { Base } from './base';

@Entity('Special')
export class Special extends Base {
  
  @Column({
    nullable: false,
    length: 500
  })
  name: string;

  @Column({
    type: 'text',
    nullable: false
  })
  description: string;

  @Column({
    type: 'timestamp with time zone',
    nullable: false
  })
  endDate: Date;

  @Column({
    type: 'timestamp with time zone',
    nullable: false
  })
  startDate: Date;
}
