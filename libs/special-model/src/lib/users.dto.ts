import { BaseDto } from './base.dto';
import { UsersToken } from './users-token';

export class UsersDto extends BaseDto {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  passwordSalt: string;
  passwordHashingAlgorithm: string;
  tokens: UsersToken[];
}
