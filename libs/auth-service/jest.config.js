module.exports = {
  name: 'auth-service',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/auth-service'
};
