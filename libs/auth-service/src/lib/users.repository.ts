//https://github.com/mdoury/nestjs-auth-starter-kit/tree/master/src/user
import { Injectable } from '@nestjs/common';
import { Users, UsersDto } from '@special-repo/special-model';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserRepository {
  constructor(
    @InjectRepository(Users) private readonly repo: Repository<Users>
  ) {}

  async findAll(relations?: string[]): Promise<Users[]> {
    return await this.repo.find({ relations: relations });
  }

  async findOneByEmail(email: string): Promise<Users> {
    const databaseUser = await this.repo.findOne({ email: email });
    return databaseUser;
  }

  async update(user: Users): Promise<Users> {
    const databaseUser = await this.repo.findOne({ id: user.id });

    user.password = databaseUser.password;
    user.passwordSalt = databaseUser.passwordSalt;
    user.passwordHashingAlgorithm = databaseUser.passwordHashingAlgorithm;

    const entity = await this.repo.save(user);
    return entity;
  }

  async validateUser(email: string, password: string): Promise<boolean> {
    const user = await this.repo.findOne({ email: email });
    if (!user) return false;
    const hasMatched = await bcrypt.compare(password, user.password);
    return !!user && hasMatched === true;
  }

  async create(user: Users): Promise<Users> {
    const timeStamp = new Date();
    //TODO: Assign loggedin user or system
    user.createBy = 'system';
    user.createDate = timeStamp;
    user.updateBy = 'system';
    user.createDate = timeStamp;
    const round = 10;
    const salt = await bcrypt.genSalt(round);
    user.passwordSalt = salt;

    const hash = await bcrypt.hash(user.password, user.passwordSalt);
    user.password = hash;

    user.passwordHashingAlgorithm = `bcrypt${round}`;
    const entity = await this.repo.save(user);

    entity.password = '';
    return entity;
  }

  async findById(id: number): Promise<Users> {
    return await this.repo.findOne({ id });
  }
  async delete(userId: number) {
    const userToDelete = await this.findById(userId);
    if (!!userToDelete) {
      await this.repo.delete({ id: userId });
    } else {
      throw Error('User not found');
    }
  }
}
//https://github.com/mdoury/nestjs-auth-starter-kit/tree/master/src/user
