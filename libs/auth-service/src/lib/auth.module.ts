import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './users.repository';
import { Users, UsersToken } from '@special-repo/special-model';
import { AuthService } from './auth.service';

@Module({
  imports: [TypeOrmModule.forFeature([Users, UsersToken])],
  providers: [UserRepository, AuthService],
  exports: [UserRepository, AuthService]
})
export class AuthModule {}
