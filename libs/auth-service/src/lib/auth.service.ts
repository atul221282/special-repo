import { Injectable } from '@nestjs/common';
import { Users, UsersToken, JwtPayload } from '@special-repo/special-model';
import { EntityManager } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from './users.repository';
@Injectable()
export class AuthService {
  constructor(
    private manager: EntityManager
  ) {}

 

  async save(): Promise<UsersToken> {
    const user = await this.getExistingUser();
    const token = new UsersToken();
    token.createBy = 'Iha';
    token.updateBy = 'Iha';
    token.createDate = new Date();
    token.updateDate = user.createDate;
    token.token = 'qwerty';
    token.user = user;

    const data: (Users | UsersToken)[] = await this.manager.save([user, token]);

    return data[1] as UsersToken;
  }

  private getNewUser(): Users {
    const user = new Users();
    user.email = 'b@b.com';
    user.firstName = 'b';
    user.lastName = 'c';
    user.password = '123456';
    user.passwordHashingAlgorithm = 'bcrtypt';
    user.passwordSalt = '54321';
    user.createBy = 'Iha';
    user.updateBy = 'Iha';
    user.createDate = new Date();
    user.updateDate = user.createDate;
    return user;
  }

  private async getExistingUser(): Promise<Users> {
    const us = await this.manager.findOne(Users, { email: 'atul221282@gmail.com' });
    return us;
  }
}
