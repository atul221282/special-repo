import { Module } from '@nestjs/common';
import { SpecialApiController } from './special-api.controller';
import { SpecialApiService } from './special-api.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Special} from '@special-repo/special-model';
@Module({
  imports: [TypeOrmModule.forFeature([Special])],
  controllers: [SpecialApiController],
  providers: [SpecialApiService]
})
export class SpecialApiModule {}
