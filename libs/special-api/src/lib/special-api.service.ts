import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Special } from '@special-repo/special-model';
@Injectable()
export class SpecialApiService {
  constructor(
    @InjectRepository(Special)
    private readonly repository: Repository<Special>
  ) {}

  async getData(): Promise<Special[]> {
    const photos = await this.repository.find();
    return photos;
  }

  async save(specialEntity: Special): Promise<Special> {
    const special = await this.repository.save(specialEntity);
    return special;
  }

  async update(specialEntity: Special): Promise<Special> {
    const special = await this.repository.save(specialEntity);
    return special;
  }
}
