import { Controller, Get, Post, Body, Put, Param } from '@nestjs/common';
import { SpecialApiService } from './special-api.service';
import { ApiUseTags, ApiOperation, ApiImplicitBody } from '@nestjs/swagger';
import { SpecialDto } from '@special-repo/special-model';
import { Observable } from 'rxjs/internal/Observable';
import { from } from 'rxjs/internal/observable/from';

@Controller('specials')
@ApiUseTags('api/specials')
export class SpecialApiController {
  constructor(private readonly specialService: SpecialApiService) {}

  @Get()
  @ApiImplicitBody({
    name: 'special entity data',
    type: SpecialDto
  })
  @ApiOperation({ title: 'get all specials' })
  async getData(): Promise<SpecialDto[]> {
    return await this.specialService.getData();
  }

  @Post()
  @ApiImplicitBody({
    name: 'special entity data',
    type: SpecialDto
  })
  @ApiOperation({ title: 'cerate new special' })
   create(@Body() specialEntity: SpecialDto): Observable<SpecialDto> {
    return from(this.specialService.save(specialEntity));
  }

  @Put(':id')
  @ApiImplicitBody({
    name: 'special entity data',
    type: SpecialDto
  })
  @ApiOperation({ title: 'update special' })
  update(
    @Param('id') id: string,
    @Body() specialEntity: SpecialDto
  ): Observable<SpecialDto> {
    return from(this.specialService.update(specialEntity));
  }
}
