import { Test, TestingModule } from '@nestjs/testing';
import { SpecialApiController } from './special-api.controller';

describe('SpecialApi Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [SpecialApiController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: SpecialApiController = module.get<SpecialApiController>(SpecialApiController);
    expect(controller).toBeDefined();
  });
});
