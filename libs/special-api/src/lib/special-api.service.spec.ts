import { Test, TestingModule } from '@nestjs/testing';
import { SpecialApiService } from './special-api.service';

describe('SpecialApiService', () => {
  let service: SpecialApiService;
  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SpecialApiService],
    }).compile();
    service = module.get<SpecialApiService>(SpecialApiService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
