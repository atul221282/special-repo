import { Injectable, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs/internal/Observable';
import { classToPlain } from 'class-transformer';
import { map } from 'rxjs/operators';

@Injectable()
export class TransformInterceptor {
  intercept(
    context: ExecutionContext,
    call$: Observable<any>
  ): Observable<any> {
    return call$.pipe(map((data: any) => classToPlain(data)));
  }
}
