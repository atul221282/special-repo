import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface
} from 'class-validator';

@ValidatorConstraint()
export class IsDateGreaterThan implements ValidatorConstraintInterface {
  validate(dateProperty: Date, validationArguments: ValidationArguments) {
    const dateToCompare =
      validationArguments.object[validationArguments.constraints[0]];
    console.log(dateToCompare);
    return dateProperty > dateToCompare;
  }
}
