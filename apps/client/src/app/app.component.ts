import { Component } from '@angular/core';

@Component({
  selector: 'special-repo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
}
