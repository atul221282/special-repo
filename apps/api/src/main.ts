/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { TransformInterceptor } from '@special-repo/common-api';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Special example')
    .setBasePath('api')
    .setDescription('The Special API description')
    .setVersion('1.0')
    .addTag('special')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('sw', app, document);

  app.setGlobalPrefix(`api`);
  app.useGlobalInterceptors(new TransformInterceptor());
  app.useGlobalPipes(new ValidationPipe({}));
  app.enableCors({
    origin: '*',
    allowedHeaders: ['Content-Type', 'Accept', 'UUID']
  });
  const port = process.env.port || 3333;

  await app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/api`);
  });
}

bootstrap();
