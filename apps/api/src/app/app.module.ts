import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpecialApiModule } from '@special-repo/special-api';
import { Special, Users, UsersToken } from '@special-repo/special-model';

import { UsersModule } from '@special-repo/user-api';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'qwert1',
      database: 'specialDB',
      synchronize: true,
      entities: [Users, Special, UsersToken]
    }),
    UsersModule,
    SpecialApiModule
  ]
})
export class AppModule {}
